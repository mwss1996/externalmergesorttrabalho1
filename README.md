### Trabalho divisão de arquivos ExternalMergeSort ###

External Merge Sort é um algoritmo para organizar arquivos com grandes volumes de dados, quando o conteúdo não cabe na memória RAM.

Esse projeto implementa parte do algoritmo (dividir em partes o arquivo original e ordenar essas partes) utilizando a linguagem Java e o arquivo de CEP's do Brasil.

Trabalho para a matéria de Organização de Estruturas de Arquivos do curso de Ciência da Computação do CEFET Maracanâ.

2º Semestre de 2017