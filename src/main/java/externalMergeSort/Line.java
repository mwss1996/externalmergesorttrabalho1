package externalMergeSort;

public class Line implements Comparable<Line> {
	
	private String text;
	
	public Line(String text) {
		this.text = text;
	}
	
	public Double getCep() {
		return Double.parseDouble(text.substring(290, 299));
	}

	@Override
	public int compareTo(Line line) {
		return getCep().compareTo(line.getCep());
	}
	
	@Override
	public String toString() {
		return text;
	}
}