package externalMergeSort;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ExternalMergeSort {
	
	static final Long LINES_IN_MEMORY = 70000l;
	
	public static void main(String[] args) throws IOException {
		
		String encoding = "ISO-8859-1";
		
		FileInputStream fileInputStream = new FileInputStream("CEP.txt");
		InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream, encoding);
		BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
		
		String currentLine = bufferedReader.readLine();
		
		Integer totalLines = 0;
		
		int numberOfFiles = 0;
		//Divide os arquivos
		do {
			PrintWriter printWriter = new PrintWriter(numberOfFiles + ".txt", encoding);
			BufferedWriter bufferedWriter = new BufferedWriter(printWriter);
			
			int counter = 0;

			List<Line> linesInMemory = new ArrayList<>();
			while(currentLine != null && counter < LINES_IN_MEMORY)
			{
				linesInMemory.add(new Line(currentLine));
				currentLine = bufferedReader.readLine();
				totalLines++;
				counter++;
			}
			
			Collections.sort(linesInMemory);
			
			for (Line lineCounter : linesInMemory) {
				bufferedWriter.write(lineCounter.toString());
				bufferedWriter.newLine();
			}

			bufferedWriter.close();
			printWriter.close();
			numberOfFiles++;
		} while (currentLine != null);
		
		bufferedReader.close();
		inputStreamReader.close();
		fileInputStream.close();
		
		System.out.println("Veja os arquivos .txt com a saida do algoritmo.");
	}
	
}